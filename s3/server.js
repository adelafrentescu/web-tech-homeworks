const express = require('express');
const bodyParser = require('body-parser');

const app = express();
app.use(bodyParser.json());
//var http=require('http');
//http.createServer(function(req, res){
  //  res.writeHead(200, {'Content-Type': 'text/plain'});
    //res.end('For testing\n');
//}).listen(process.env.PORT);
let products = [
    {
        id: 0,
        productName: 'Samsung Galaxy S9',
        price: 4000
    },
    {
        id: 1,
        productName: 'Iphone XR',
        price: 3999
    }
];

app.get('/get-all', (req, res) => {
    res.status(200).send(products);
})

app.post('/add', (req, res) => {
    if(req.body.productName && req.body.price){
        let product = {
            id: products.length,
            productName: req.body.productName,
            price: req.body.price
        };
        products.push(product);
        res.status(200).send(product);
    } else {
        res.status(500).send('Error!');
    }
});

app.put('/id', (req, res) =>{
    const id = req.params.id;
    let update = false;
    products.forEach((product) =>{
        if(product.id == id){
            update = true;
            product.price = req.body.price;
            product.productName = req.body.productName;
        }
    });
    if(update){
        res.status(200).send(`Product ${id} is updated!`)
    }else{
        res.status(404).send(`There is no product with id ${id}`);
    }
})

app.delete('/products/:productName/delete', (req, res)=>{
    const name = req.params.productName;
    if(name === products.productName){
        res.delete(name);
      
        res.status(200).send(`The product with the name ${name} was deleted`);
        
    }else{
        res.status(404).send(`No product found with this name`);
    }
});
app.listen(8080, () => {
    console.log('Server started on port 8080...');
});