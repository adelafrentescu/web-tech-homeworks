import React from 'react';

export class ProductList extends React.Component{
    constructor(props){
        super(props);
        this.state={
            source: []
        };
    }

    render(){
        let items = this.props.source.map((item, index) => 
        <div key={index}>{"Product name: " + item.productName + " price: " + item.price}</div>
        );
        return(
            <div>
                <h1>{this.props.title}</h1>
                <div className="Products">
                {items}
                </div>
            </div>
        )
    }

}